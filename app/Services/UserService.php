<?php 
namespace App\Services;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Client;
use App\Services\BaseService;
use App\Services\GoogleMapService;
use App\Repositories\UserRepository;
use App\Repositories\ClientRepository;
use Illuminate\Support\Facades\Hash;

class UserService extends BaseService
{
	protected $userRepository;

	/**
	 * Get a constuctor.
	 *
	 * @return array
	 */
	public function __construct(User $userModel, Client $clientModel, GoogleMapService $googleMapService)
	{
		$this->userRepository = new UserRepository($userModel);
		$this->clientRepository = new ClientRepository($clientModel);
		$this->googleMapService = $googleMapService;
		parent::__construct($this->userRepository);
	}

	/**
	 * add User.
	 *
	 * param $request
	 *
	 * @return array
	*/
	public function addUser($request)
	{
		// Fetch Latitude and Longitude based on address
		$latitude = $longitude = null;
		$coordinates = $this->googleMapService->getLatLong($request);
		if (!empty($coordinates)) {
			$latitude = $coordinates['lat'];
			$longitude = $coordinates['lng'];
		}

		// Set Default Start and End Validity
		$startValidity = ($request->has('startValidity') && !empty($request['startValidity'])) ? $request['startValidity'] : Carbon::now();
		$endValidity = ($request->has('endValidity') && !empty($request['endValidity'])) ? $request['endValidity'] : Carbon::now()->addDays(15);

		// Prepare client data and store in database
		$data = [
			'client_name' => $request['name'], 
			'address1' => $request['address1'], 
			'address2' => $request['address2'], 
			'city' => $request['city'], 
			'state' => $request['state'], 
			'country' => $request['country'], 
			'latitude' => $latitude, 
			'longitude' => $longitude, 
			'zip' => $request['zipCode'], 
			'phone_no1' => $request['phoneNo1'], 
			'phone_no2' => $request['phoneNo2'], 
			'start_validity' => Carbon::parse($startValidity)->format('Y-m-d'), 
			'end_validity' => Carbon::parse($endValidity)->format('Y-m-d'), 
			'status' => 'Active'
		];
		$clientId = $this->clientRepository->addClient($data);
		if ($clientId) {
			// Prepare user data and store in database
			$userData = [
				'client_id' => $clientId, 
				'first_name' => $request['user']['firstName'], 
				'last_name' => $request['user']['lastName'], 
				'email' => $request['user']['email'], 
				'password' => Hash::make($request['user']['password']), 
				'phone' => $request['user']['phone'], 
				'last_password_reset' => Carbon::now(), 
				'status' => 'Active'
			];
			$userId = $this->userRepository->addUser($userData);

			return [
				"status" => "success",
				"message" => "Registration Completed.",
				"client_id" => $clientId, 
				"user_id" => $userId
			];
		}
		return $clientId;
	}

	/**
	 * Check if user exists by email
	 *
	 * param $email
	 *
	 * @return array
	*/
	public function isUserExist($email)
	{
		return $this->userRepository->getUser($filter = ['email' => $email]);
	}

	/**
	 * return all users
	 *
	 * @return array
	*/
	public function getUsers()
	{
		return $this->userRepository->getAllUsers();
	}
}
