<?php

namespace App\Services;
use Spatie\Geocoder\Facades\Geocoder;
use Illuminate\Support\Facades\Redis;

class GoogleMapService
{

      /**
      * Get Latitude and Longitude by address
      *
      * @return array
      */
      public function getLatLong($request) 
      {
          $address = $request['address1'].','.$request['address2'].','.$request['city'].','.$request['state'].','.$request['country'];

          // Check and return if coordinates are already stored in redis cache
          if ($coordinates = Redis::get('address_'.$address)) {
            return json_decode($coordinates, true);
          }

          Geocoder::setApiKey(config('geocoder.key'));

          Geocoder::setCountry(config('geocoder.country', 'US'));

          $coordinates = Geocoder::getCoordinatesForAddress($address);

          Redis::set('address_'.$address, json_encode($coordinates));

          return $coordinates;
    }
}