<?php 
namespace App\Services;

use App\Models\Client;
use App\Services\BaseService;
use App\Repositories\ClientRepository;
use Illuminate\Support\Facades\Hash;

class ClientService extends BaseService
{
	protected $clientRepository;

	/**
	 * Get a constuctor.
	 *
	 * @return array
	 */
	public function __construct(Client $clientModel)
	{
		$this->clientRepository = new ClientRepository($clientModel);
		parent::__construct($this->clientRepository);
	}

	/**
	 * Return All Clients
	 *
	 * @return array
	*/
	public function findAll($filter, $orderBy, $orderDirection, $limit)
	{
		return $this->clientRepository->findAll($filter, $orderBy, $orderDirection, $limit);
	}
}
