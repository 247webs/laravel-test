<?php 
namespace App\Services;

use Guzzle\Service\Exception\CommandTransferException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Redis;

class BaseService
{
    protected $repository;
    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of single user.
     *
     * @param $data, $id
     *
     * @return array
     */
    public function withSingle($data, $id)
    {
        return $this->userRepository->withSingle($data, $id);
    }

    /**
     * File Upload functions.
     *
     * @param $file, $bucket, $path
     *
     * @return array
     */
    public function fileUpload($file)
    {

    }

    /**
     * Remove cache key
     *
     * @param $data
     *
     * @return array
     */
    public function removeKeys($data)
    {
        $keys = Redis::keys('*'.$data.'*');
        if (!empty($keys) ) {
            $keys = array_map(
                function ($k) {
                    return str_replace('stryds_', '', $k);
                }, $keys
            );
            Redis::del($keys);
        }
    }
}
