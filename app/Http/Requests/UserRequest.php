<?php 
namespace App\Http\Requests;

use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function validate($request)
    {
		$rules = [
			'name' => 'required',
			'address1' => 'required',
			'address2' => 'required',
			'city' => 'required',
			'state' => 'required',
			'country' => 'required',
			'zipCode' => 'required',
			'phoneNo1' => 'required',
			'user.firstName' => 'required',
			'user.lastName' => 'required',
			'user.email' => 'required|email',
			'user.password' => 'min:6|required_with:user.passwordConfirmation|same:user.passwordConfirmation',
			'user.passwordConfirmation' => 'min:6',
			'user.phone' => 'required',
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails($validator)) {
			return ['message' => $validator->errors()];
		}
	}
}
