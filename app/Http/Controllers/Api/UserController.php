<?php
namespace App\Http\Controllers\Api;

use App\Http\HttpCode;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	protected $userService;
	protected $userRequest;

	/**
	 * Create a new UserController instance.
	 *
	 * @return void
	 */
	public function __construct(UserService $userService, UserRequest $userRequest)
	{
		$this->userService = $userService;
		$this->userRequest = $userRequest;
	}

	/**
	 * post user.
	 *
	 * param $request
	 *
	 * @return response
	*/
	public function postUser(Request $request)
	{
		$validator = $this->userRequest->validate($request);
		
		if (isset($validator['success']) && $validator['success'] == false) {
			return response()->json(['message' => $validator['message']], HttpCode::UNPROCESSABLE_ENTITY);
		}

		$user = $this->userService->isUserExist($request['user']['email']);
		if (!empty($user)) {
			return response()->json(['message' => 'Email already exist!'], HttpCode::UNPROCESSABLE_ENTITY);
		}

		$response = $this->userService->addUser($request);

		return response()->json(['data' => $response], HttpCode::OK);
	}
}
