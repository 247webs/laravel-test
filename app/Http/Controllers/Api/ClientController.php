<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\ClientService;
use Illuminate\Http\Request;
use App\Http\HttpCode;

class ClientController extends Controller
{
	/**
	 * Create a new ClientController instance.
	 *
	 * @return void
	 */
	public function __construct(ClientService $clientService)
	{
		$this->clientService = $clientService;
	}

	/**
	 * Get List of Clients
	 */
	public function getClients(Request $request){
        
		$requestParams = $request->all();

		// Handle filter, sorting, pagination data
		$orderBy = $orderDirection = $filter = $limit = null;
		
		if(isset($requestParams['order_by'])){
			$orderBy = $requestParams['order_by'];
			unset($requestParams['order_by']);
		}

		if(isset($requestParams['order_direction'])){
			$orderDirection = $requestParams['order_direction'];
			unset($requestParams['order_direction']);
		}

		if(isset($requestParams['limit'])){
			$limit = $requestParams['limit'];
			unset($requestParams['limit']);
		}

		if(isset($requestParams['page'])){
			$page = $requestParams['page'];
			unset($requestParams['page']);
		}

		// Remaining request params can be used as filters
		$filter = $requestParams;

		$response = $this->clientService->findAll($filter, $orderBy, $orderDirection, $limit);
		
		return response()->json($response, HttpCode::OK);
    }
}
