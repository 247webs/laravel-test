<?php 
namespace App\Repositories;

use DB;
use App\Models\Client;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\ClientCollection;


class ClientRepository
{
	protected $model;

	public function __construct(Client $model)
	{
		$this->model = $model;
	}

	/**
	 * Return All Clients
	 *
	 * @return array
	*/
	public function findAll($filters = [], $orderBy = null, $orderDirection = null, $limit = null)
	{
		$queryUser = $this->model::query();
		$queryUser->leftJoin('users', 'clients.id', '=', 'users.client_id');
		$queryUser->select(array('clients.*', DB::raw('COUNT(users.id) as total_users') ));
		$queryUser->selectRaw("count(case when users.status = 'Active' then 1 end) as active_users");
		$queryUser->selectRaw("count(case when users.status = 'InActive' then 1 end) as inactive_users");
		$queryUser->groupBy('clients.id');

		if (!empty($filters)) {
			$queryUser->where($filters);
		}

		/* 
		 * Applying Sorting, Filtering and Pagination
		 **/
		if($orderBy === null) {
			$orderBy = "id";
		}

		if($orderDirection === null) {
			$orderDirection = "DESC";
		}

		if ($limit === null) {
			$limit = 20;
		}

		$queryUser->orderBy($orderBy, $orderDirection);

		//Fetch list of results
		return $queryUser->paginate($limit);
	}

	/**
	 * add client details.
	 * 
	 * param $data
	 *
	 * @return array
	 */
	public function addClient($data)
	{
		try {
			return $this->model->insertGetId($data);
		}
		catch (\Exception $e)
		{
			return false;
		}
	}
}