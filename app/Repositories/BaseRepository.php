<?php 
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use DB;

class BaseRepository
{

	protected $model;
	const DEFAULT_LIMIT = 50;

	public function __construct(Model $model)
	{
		$this->model = $model;
	}

}
