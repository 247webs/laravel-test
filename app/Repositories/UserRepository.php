<?php 
namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class UserRepository
{
	protected $model;

	public function __construct(User $model)
	{
		$this->model = $model;
	}

	/**
	 * Add user.
	 * 
	 * @return array
	 */
	public function addUser($userData)
	{
		try {
			return $this->model->insertGetId($userData);
		}
		catch (\Exception $e)
		{
			return false;
		}
	}

	/**
     * Place Search on Google-Map functions.
     *
     * @param $address
     *
     * @return array
     */
    public function getUser($filter)
    {
		return $this->model->where($filter)->first();
	}
}
