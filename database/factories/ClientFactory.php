<?php

namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'client_name' => "Direct Ad Network",
            'address1' => "Rock Heven Way",
            'address2' => "125",
            'city' => "Sterling",
            'state' => "VA",
            'country' => "USA",
            'zip' => "20166",
            'phone_no1' => "555-666-7777",
            'User' => [
                "first_name" => "John",
                "last_name" => "Doe",
                "email" => "john.doe@example.com",
                "password" => "secret@123",
                "phone" => "123-456-7890"
            ]
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
