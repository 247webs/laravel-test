<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Client;

class ClientTest extends TestCase
{
    private $json = [];

    # Test function for create client
    public function testCreateClientContents()
    {
        $client = new Client();
        $client->fill(['name'=>true]);
        $this->assertTrue($client->name);
        $this->assertNull($client->email);
    }
}
