<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;

class UserTest extends TestCase
{
    public function testCreateUser()
    {
        $data = [
            'name' => 'testing',
            'address1' => 'Kharakua',
            'address2' => 'Ujjain',
            'city' => 'Ujjain',
            'state' => 'Madhya pradesh',
            'country' => 'India',
            'zipCode' => '456005',
            'phoneNo1' => '123456',
            'phoneNo2' => '123456',
            'user' => [
                'firstName' => 'ddd',
                'lastName' => 'Deo',
                'email' => rand(999,9999).'@gmail.com',
                'password' => '123456',
                'passwordConfirmation' => '123456',
                'phone' => '1234567890',
            ],
        ];
        $user = User::factory()->create();
        $response = $this->actingAs($user, 'api')->json('POST', '/api/post-user', $data);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['status' => "success"]]);
        $response->assertJson(['data' => ['message' => "Registration Completed."]]);
    }

    public function testGettingAllClients()
    {
        $response = $this->json('GET', '/api/get-clients?order_by=id&order_direction=asc&limit=5');
        $response->assertStatus(200);

        $response->assertJsonStructure(
            [
                'data' => [
                        [
                                'id',
                            'client_name',
                            'address1',
                            'address2',
                            'city',
                            'state',
                            'country',
                            'latitude',
                            'longitude',
                            'phone_no1',
                            'phone_no2',
                            'zip',
                            'start_validity',
                            'end_validity',
                            'status',
                            'created_at',
                            'updated_at',
                            'deleted_at',
                            'total_users',
                            'active_users',
                            'inactive_users'
                        ]
                ],
                'current_page',
                'first_page_url',
                'last_page_url',
                'from',
                'to',
                'last_page',
                'next_page_url',
                'prev_page_url',
                'per_page',
                'total',
                'links'
            ]
        );
    }
}